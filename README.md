# i5-mspr-qualite-donnees-dashboard

## Installation:

``
git clone https://gitlab.com/chevallm2/i5-mspr-qualite-donnees-dashboard.git
cd i5-mspr-qualite-donnees-dashboard
npm install
``

## Lancement de l'application:

``
ng serve
``

